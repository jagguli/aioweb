import logging
from .model import Model
from .couchdb import CouchDBAdapter
try:
    from .mongodb import MongoDBAdapter
except ImportError:
    logging.warn("Pymongo is not installed ?")


def get_db(config=None):
    if not config:
        from aioweb.config import config
    dbtype = config['database'].get('driver', 'couchdb')
    if dbtype == 'mongodb':
        return MongoDBAdapter(config['database']['dbname'])
    else:
        return CouchDBAdapter(
            'http://%(username)s:%(password)s@localhost:5984/' %
            config['database'], config['database']['dbname'],)

